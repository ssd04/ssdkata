﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using graf_ed.Entitati;

namespace graf_ed
{
    public partial class Form1 : Form
    {
        List<Nod> nodList = new List<Nod>();
        List<PictureBox> pictureList = new List<PictureBox>();

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void b_save_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog1.FileName, textBox1.Text);
            }
        }

        private void b_import_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label1.Text = openFileDialog1.FileName;
                textBox1.Text = File.ReadAllText(label1.Text);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void b_add_nod_Click(object sender, EventArgs e)
        {
            Nod currentNod = new Nod();
            currentNod.NrNod++;

            //Image image = Image.FromFile(@"\myimage.png");
            PictureBox pictureBox1 = new PictureBox();
            pictureBox1.Location = new System.Drawing.Point(56, 38);
            pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(38, 41);
            pictureBox1.TabIndex = 0;
            pictureBox1.TabStop = false;
            //pictureBox1.
            
            pictureBox1.ImageLocation = "D:\\darius\\Visma Training\\proiect ebs\\editor-de-grafuri\\myimage.png";
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Size = new System.Drawing.Size(30, 30);
            pictureBox1.BackColor = Color.Transparent;

            

            pictureList.Add(pictureBox1);
            nodList.Add(currentNod);
            splitContainer3.Panel1.Controls.Add(pictureBox1);


        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void verifyButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("picture: " + pictureList.Count + "nod: " + nodList.Count);
        }

        private void restToolStripMenuItem_Click(object sender, EventArgs e)
        {
            nodList.Clear();

            foreach (var picture in pictureList)
            {
                picture.Image = null;
            }

            pictureList.Clear();
            
        }

    }
}