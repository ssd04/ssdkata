﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace graf_ed.Entitati
{
    public class Graf 
    {
        Nod nod = new Nod();
        

        private List<Nod> GenerareNod(int nrNoduri)
        {
            List<Nod> ListaNod = new List<Nod>();

            for (int i = 0; i < nrNoduri; i++)
            {
                Nod nod = new Nod();
                nod.PY = 100 + (i / 7) * 60;
                nod.PX = 20 + (i % 7) * 60;
                nod.NrNod = 1;
                ListaNod.Add(nod);
            }
            return ListaNod;
        }

        private void AfisareNod(List<Nod> ListaNod)
        {
            foreach (var nod in ListaNod)
            {
                Label namelabel = new Label();
                namelabel.Text = nod.NrNod.ToString();
                namelabel.AutoSize = true;
                namelabel.BackColor = System.Drawing.Color.Transparent;
                namelabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                namelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular);
                namelabel.ForeColor = System.Drawing.Color.Black;
                namelabel.Location = new System.Drawing.Point(nod.PX, nod.PY);
                namelabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                //this.Controls.Add(namelabel);
            }
        }
        
    }
}
