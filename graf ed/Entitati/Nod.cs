﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace graf_ed.Entitati
{
    public class Nod
    {
        private int nrNod;

        public int NrNod
        {
            get { return this.nrNod; }
            set { this.nrNod = value; }
        }
        private int px;

        public int PX
        {
            get { return px; }
            set { px = value; }
        }
        private int py;

        public int PY
        {
            get { return py; }
            set { py = value; }
        }
    }
}
